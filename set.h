#ifndef SET_SET_H
#define SET_SET_H

#include <memory>  // shared_ptr
#include <tuple>   // pair
#include <cassert> // assert
#include <optional>

template<typename T, typename Allocator = std::allocator<T>>
class set : public Allocator {
public:
    using value_type = typename set<T>::value_type;
    using size_type = typename set<T>::size_type;
    using pointer = typename set<T>::pointer;
    using reference  = typename set<T>::reference;
    using const_reference  = typename set<T>::const_reference;
    struct iterator;
    using const_iterator = iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

private:
    static bool less(std::optional<value_type> const &a, std::optional<value_type> const &b) {
        assert((a or b) && "less compares two empty optionals");

        if (a and not b) { return true; }
        else if (not a and b) { return false; }
        else { return *a < *b; }
    }

    static bool less_eq(std::optional<value_type> const &a, std::optional<value_type> const &b) {
        return less(a, b) or a == b;
    }

    struct Node {
        std::optional<value_type> data;
        std::unique_ptr<Node> left = nullptr;
        std::unique_ptr<Node> right = nullptr;
        Node *parent = nullptr;

        Node() noexcept : data{} {}

        explicit Node(value_type data) : data(data) {}

        static std::unique_ptr<Node> const &
        append(const_reference value, Node *predecessor) {
            assert(predecessor);

            std::unique_ptr<Node> single = std::make_unique<Node>(value);
            single->parent = predecessor;
            predecessor->right.swap(single);

            return predecessor->right;
        }

        static std::unique_ptr<Node> const &
        prepend(const_reference value, Node *successor) {
            assert(successor);

            std::unique_ptr<Node> single = std::make_unique<Node>(value);
            single->parent = successor;
            successor->left.swap(single);

            return successor->left;
        }

        static Node *
        lower_bound(Node *root, const_reference value) {
            if (less(root->data, value)) {
                if (root->right) { return lower_bound(root->right.get(), value); }
                else { return (++iterator(root)).base; }
            } else {
                if (root->left) { return lower_bound(root->left.get(), value); }
                else { return root; }
            }
        }

        static Node *
        upper_bound(Node *root, const_reference value) {
            if (less_eq(root->data, value)) {
                if (root->right) { return upper_bound(root->right.get(), value); }
                else { return (++iterator(root)).base; }
            } else {
                if (root->left) { return upper_bound(root->left.get(), value); }
                else { return root; }
            }
        }
    };

public:
    struct iterator {
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = set::value_type;
        using difference_type = int;
        using pointer = value_type const *;
        using reference = value_type const &;

        friend set;

    private:
        Node *base;

        explicit iterator(Node *other) : base(other) {}

    public:
        iterator() : base(nullptr) {};

        iterator &operator++() {
            assert(base);

            if (base->right) {
                base = base->right.get();
                while (base->left) { base = base->left.get(); }
            } else {
                auto next = base->parent;
                while (next and less(next->data, base->data)) {
                    base = next;
                    next = base->parent;
                }
                base = next;
            }
            return *this;
        }

        iterator operator++(int) {
            auto copy = *this;
            ++*this;
            return copy;
        }

        iterator &operator--() {
            assert(base);

            if (base->left) {
                base = base->left.get();
                while (base->right) { base = base->right.get(); }
            } else {
                auto next = base->parent;
                while (next and less(base->data, next->data)) {
                    base = next;
                    next = base->parent;
                }
                base = next;
            }
            return *this;
        }

        iterator operator--(int) {
            auto copy = *this;
            --*this;
            return copy;
        }

        reference operator*() const noexcept {
            assert(base);
            return *base->data;
        }

        pointer operator->() const noexcept { return &operator*(); }

        bool operator==(iterator other) const noexcept { return base == other.base; }

        bool operator!=(iterator other) const noexcept { return not(*this == other); }
    };

private:
    std::pair<iterator, bool> insert(iterator pos, const_reference value) {
        if (pos != end() and *pos == value) {
            return {pos, false};
        } else {
            Node *result_base;
            if (not pos.base->left) {
                result_base = Node::prepend(value, pos.base).get();
            } else {
                result_base = Node::append(value, std::prev(pos).base).get();
            }
            if (pos == begin()) { first = result_base; }
            return {iterator{result_base}, true};
        };
    }

    void fix() noexcept {
        if (fake_end.left) { fake_end.left->parent = &fake_end; }
        last = &fake_end;
        root = last;
        while (root->parent) { root = root->parent; }
        first = root;
        while (first->left) { first = first->left.get(); }
    }

public:
    set() = default;

    set(set const &other) : set() {
        auto pos = end();
        for (auto it = other.rbegin(); it != other.rend(); ++it) { pos = insert(pos, *it).first; }
    }

    set &operator=(set rhs) noexcept {
        swap(rhs);
        return *this;
    }

    std::pair<iterator, bool> insert(const_reference value) {
        auto pos = lower_bound(value);
        return insert(pos, value);
    }

    iterator erase(const_iterator pos) {
        assert(pos != end());

        auto base = pos.base;
        auto parent = base->parent;
        assert(parent);
        auto &left = base->left;
        auto &right = base->right;
        auto &child = less(base->data, parent->data) ? parent->left : parent->right;
        auto next = std::next(pos);

        if (not left) {
            child.swap(right);
            base->right = nullptr;
        } else {
            child.swap(left);

            Node *rightmost = child.get();
            while (rightmost->right) { rightmost = rightmost->right.get(); }
            if (right) { right->parent = rightmost; }
            rightmost->right.swap(right);

            base->left = nullptr;
        }
        if (child) { child->parent = parent; }

        if (pos == begin()) { first = next.base; }

        return next;
    }

    void clear() noexcept { first = root = last; }

    const_iterator lower_bound(const_reference value) const {
        return const_iterator{Node::lower_bound(root, value)};
    }

    const_iterator upper_bound(const_reference value) const {
        return const_iterator{Node::upper_bound(root, value)};
    };

    const_iterator find(const_reference value) const {
        auto pos = lower_bound(value);
        if (pos == end() or *pos != value) { return end(); }
        else { return pos; }
    }

    iterator begin() noexcept { return iterator{first}; }

    iterator end() noexcept { return iterator{last}; };

    const_iterator begin() const noexcept { return iterator{first}; }

    const_iterator end() const noexcept { return iterator{last}; };

    reverse_iterator rbegin() noexcept { return reverse_iterator{end()}; }

    reverse_iterator rend() noexcept { return reverse_iterator{begin()}; }

    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator{end()}; }

    const_reverse_iterator rend() const { return const_reverse_iterator{begin()}; }

    bool empty() const noexcept { return begin() == end(); }

    void swap(set &other) noexcept {
        std::swap(fake_end, other.fake_end);
        fix();
        other.fix();
    }

private:
    Node fake_end{};
    Node *last = &fake_end;
    Node *root = last;
    Node *first = last;
};

template<typename T>
void swap(set<T> &one, set<T> &two) noexcept { one.swap(two); }

#endif //SET_SET_H
